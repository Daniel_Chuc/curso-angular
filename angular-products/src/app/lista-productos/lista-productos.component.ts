import { Component, OnInit } from '@angular/core';
import { agreproducto } from './../models/agre-producto.model';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {
  productos: agreproducto[];
  constructor() {
  this.productos = [];
   }

  ngOnInit(): void {
  }

guardar(nombre:string, Categoria:string, Precio:string, Descripcion:string):boolean {
this.productos.push(new agreproducto(nombre,Categoria,Precio,Descripcion));
	console.log(this.productos);

	return false
}

}
