import { Component, OnInit, Input,HostBinding } from '@angular/core';
import { agreproducto } from './../models/agre-producto.model';
@Component({
  selector: 'app-agre-producto',
  templateUrl: './agre-producto.component.html',
  styleUrls: ['./agre-producto.component.css']
})
export class AgreProductoComponent implements OnInit {
@Input() producto: agreproducto;
@HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() {}

  ngOnInit(): void {
  }

}
